package br.com.alura.forum.controller.dto;

import br.com.alura.forum.modelo.Resposta;

import java.time.LocalDateTime;

public class RespostaDto {
    private Long id;
    private String mensagem;
    private LocalDateTime dataCriacao;
    private String nomeDoAutor;

    public RespostaDto(Resposta resposta) {
        id = resposta.getId();
        mensagem = resposta.getMensagem();
        dataCriacao = resposta.getDataCriacao();
        nomeDoAutor = resposta.getAutor().getNome();
    }

    public Long getId() {
        return id;
    }

    public String getMensagem() {
        return mensagem;
    }

    public LocalDateTime getDataCriacao() {
        return dataCriacao;
    }

    public String getNomeDoAutor() {
        return nomeDoAutor;
    }
}
