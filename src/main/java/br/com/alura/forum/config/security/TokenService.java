package br.com.alura.forum.config.security;

import br.com.alura.forum.modelo.Usuario;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Date;

@Service
public class TokenService {

    private final String expiration;
    private final SecretKey key;

    @Autowired
    public TokenService(@Value("${forum.jwt.secret}") String secret,
                        @Value("${forum.jwt.expiration}") String expiration) {
        key = Keys.hmacShaKeyFor(secret.getBytes(StandardCharsets.UTF_8));
        this.expiration = expiration;
    }

    public String gerarToken(Authentication authentication) {
        Usuario logado = (Usuario) authentication.getPrincipal();
        Instant now = Instant.now();
        Date dataEmissao = Date.from(now);
        Date dataExpiracao = Date.from(now.plusMillis(Long.parseLong(expiration)));
        System.out.println(logado);
        return Jwts.builder()
                .setIssuer("API do forum Alura")
                .setSubject(logado.getId().toString())
                .setIssuedAt(dataEmissao)
                .setExpiration(dataExpiracao)
                .signWith(key)
                .compact();
    }

    public Boolean isTokenValido(String token) {
        try {
            System.out.println(token);
            Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token);
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public Long getIdUsuario(String token) {
        Claims claims = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();
        return Long.parseLong(claims.getSubject());
    }
}
